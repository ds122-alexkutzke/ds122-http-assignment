Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS122 - Desenvolvimento de Aplicações Web 1*

Prof. Alexander Robert Kutzke

# Atividade: Mensagens HTTP

O servidor `web1.kutzke.com.br` está preparado para responder requisições baseadas no padrão [REST](https://developer.mozilla.org/en-US/docs/Glossary/REST).

Utilize o comando `curl` para interagir com o servidor `web1.kutzke.com.br` e alterar os registro por ele armazenados.

Ao final, preencha no arquivo `README.md`, cada uma dos comandos utilizados e a respectiva saída contendo as mensagens de requisição e resposta (é só colar a saída do comando `curl`).

O site responde às seguintes requisições:

| Verbo HTTP | Padrão UR   | Função                              |
|-----------:|-------------|-------------------------------------|
|        GET | /posts      | Lista posts (index)                 |
|       POST | /posts      | Cria um novo post (create)          |
|        GET | /posts/id   | Exibe o post número "id" (show)     |
|        PUT | /posts/id   | Altera o post número "id" (update)  |
|     DELETE | /posts/id   | Remove o post número "id" (delete)  |

## Exemplo com o método GET

```bash
curl -X GET -v web1.kutzke.com.br/posts   
```

## Respostas

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### GET (listar)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### POST (criar)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### GET com id (mostrar um registro específico)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### PUT com id (alterar um registro específico)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

### DELETE com id (apagar um registro específico)

```
COLE AQUI O COMANDO curl UTILIZADO
```

```
COLE AQUI A SAÍDA DO COMANDO curl UTILIZADO
```

